package com.multidatasource.test01.service;

import com.multidatasource.entity.UserEntity;
import com.multidatasource.test01.mapper.UserTest1Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/12/4 10:50
 * @version: 1.0
 */
@Service
//@Transactional(transactionManager = "test1TransactionManager")
public class UserTest1Service {

    @Autowired
    UserTest1Mapper userTest1Mapper;

    //指定事务，不然服务不知道使用哪个事务（高版本springboot可能不需要加也能区分）
    @Transactional(transactionManager = "test1TransactionManager")
    public List<UserEntity> findUsers(){
        return userTest1Mapper.findUser();
    };

    @Transactional(transactionManager = "test1TransactionManager")
    public int insertUser(UserEntity userEntity){
        return userTest1Mapper.insertUser(userEntity);
    }


}
