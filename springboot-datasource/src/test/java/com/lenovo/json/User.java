package com.lenovo.json;

import lombok.Data;

/**
 * @Author: CPX
 * @Date: 2020/11/17 13:32
 * @version: 1.0
 */
@Data
public class User {
    public User(String username, int age, String sex) {
        this.username = username;
        this.age = age;
        this.sex = sex;
    }

    private String username;
    private int age;
    private String sex;

}
