package com.mybatiss.service;

import com.mybatiss.MapperScanMapper.UserMapperScan;
import com.mybatiss.mapper.UserMapper;
import com.mybatiss.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/11/19 10:20
 * @version: 1.0
 */
@Service
public class UserService{

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserMapperScan userMapperScan;

    public User findById(String id) {
        return userMapper.findById(id);
    }

    public User fineByIdAndName(String id, String name) {
        return userMapper.fineByIdAndName(id, name);
    }

    public List<User> findUsers() {
        return userMapper.findUsers();
    }

    public List<User> findUsersById(String id){
        return userMapper.findUsersById(id);
    }

    public int insertUser(User user){
        return userMapper.insertUser(user);
    }

    public void updateUser(String des, int id){
        System.out.println(des);
        System.out.println(id);
        userMapper.updateUser(des, id);
    }

    public List<User> findUsers2(){
        return userMapperScan.findUsers();
    }
}
