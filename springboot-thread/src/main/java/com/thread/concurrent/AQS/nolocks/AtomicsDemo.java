package com.thread.concurrent.AQS.nolocks;

import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: CPX
 * @Date: 2021/1/14 14:12
 * @version: 1.0
 */
public class AtomicsDemo {

    private final AtomicInteger counter = new AtomicInteger(0);

    public int getValue() {
        return counter.get();
    }
    public void increment() {
        for (int i = 0; i < 3; i++) {
            int existingValue = getValue();
            int newValue = existingValue + 1;
            System.out.println(counter.compareAndSet(existingValue, newValue));
        }

    }

    @Test
    public void test(){
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("1------------start");
                increment();
                System.out.println("1------------end");
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("2------------start");
                try {
                    for (int i = 0; i < 10; i++) {
                        TimeUnit.SECONDS.sleep(1);
                        increment();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("2-------------end");
            }
        });
        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("3------------start");
                increment();
                System.out.println("3------------end");
            }
        });
        Thread thread4 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("4------------start");
                increment();
                System.out.println("4------------end");
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
    }

    @Test
    public void test2(){
        AtomicInteger atomicInteger = new AtomicInteger(3);

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("1:"+atomicInteger.get());
                System.out.println(atomicInteger.compareAndSet(atomicInteger.get(), 4));
            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("2:"+atomicInteger.get());
                System.out.println(atomicInteger.compareAndSet(atomicInteger.get(), 5));

            }
        });

        try {
            thread1.start();
            thread1.join();
            thread2.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
