package com.multidatasource.controller;

import com.multidatasource.entity.UserEntity;
import com.multidatasource.test01.service.UserTest1Service;
import com.multidatasource.test02.service.UserTest2Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/12/4 10:55
 * @version: 1.0
 */

@RestController
public class UserController {
    @Autowired
    UserTest1Service userTest1Service;

    @Autowired
    UserTest2Service userTest2Service;

    @RequestMapping("/findUsers1")
    public List<UserEntity> findUsers(){
        List<UserEntity> users = userTest1Service.findUsers();
        return users;
    }

    @RequestMapping("/insertUser1")
    public int insertUser1(){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(55);
        userEntity.setAge(22);
        userEntity.setName("zhangsan");
        userEntity.setDes("aaa");
        return userTest1Service.insertUser(userEntity);
    }

    @RequestMapping("/insertUser2")
    public int insertUser2(){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(55);
        userEntity.setAge(22);
        userEntity.setName("lisi");
        userEntity.setDes("aaa");
        return userTest2Service.insertUser2(userEntity);
    }
}
