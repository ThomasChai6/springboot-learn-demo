package com.springboot.Annotations.QualifierAnnotation;


/**
 * @Author: CPX
 * @Date: 2020/11/16 17:03
 * @version: 1.0
 */

public interface UserInterface {

    public String getUser();
}
