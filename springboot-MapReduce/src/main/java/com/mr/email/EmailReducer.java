package com.mr.email;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.Iterator;

/**
 * MultipleOutputs的使用：可以将数据根据一定条件输入到相应的文件（可以将数据输入到多个文件系统中去）
 */
public class EmailReducer extends Reducer<Text, LongWritable,Text,LongWritable> {
    MultipleOutputs<Text,LongWritable> multipleOutputs = null;
   /* wolys@21cn.com
    zss1984@126.com
  294522652@qq.com
    simulateboy@163.com
    zhoushigang_123@163.com
    sirenxing424@126.com
    lixinyu23@qq.com
    chenlei1201@gmail.com
  370433835@qq.com
    cxx0409@126.com
    viv093@sina.com
    q62148830@163.com
  65993266@qq.com
    summeredison@sohu.com
    zhangbao-autumn@163.com
    diduo_007@yahoo.com.cn
    fxh852@163.com*/
    @Override
    protected void reduce(Text key, Iterable<LongWritable> values, Context context) throws IOException, InterruptedException {

        Configuration conf = new Configuration();

        if(multipleOutputs==null){
           multipleOutputs = new MultipleOutputs<Text, LongWritable>(context);

        }


        Iterator<LongWritable> iterator = values.iterator();
        Long sum = 0L;
        while (iterator.hasNext()){
            sum+=new Long(iterator.next().toString());
            String path = "";
            String emailType = "";
            int begin = key.toString().indexOf("@");
            int end = key.toString().indexOf(".");
            if(end>begin){
//                emailType = key.toString().substring(begin+1, end);
//                System.out.println("emailType:"+emailType);
//                path=emailType;
                path = "D:\\OutputPath\\email\\"+key.toString().substring(begin+2, end)+ Math.random();
            }
            Path path1 = new Path("D:\\OutputPath\\email" + path);
            FileSystem fileSystem = path1.getFileSystem(conf);
            if(fileSystem.isDirectory(path1)){
                fileSystem.delete(path1,true);
            }
            System.out.println("path:"+path);
            multipleOutputs.write(key,new LongWritable(sum),path);
//            context.write(key,new LongWritable(sum));

        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        multipleOutputs.close();
    }
}
