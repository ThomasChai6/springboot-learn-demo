package com.thread.threadsafe;

/**
 * 并发编程导致线程安全的原因之一：由于CPU缓存导致共享变量可见性问题
 *
 * @Author: CPX
 * @Date: 2021/1/11 14:18
 * @version: 1.0
 */
public class ThreadSafeCache {

    private long count = 0;

    //对count的值累加1000次
    private void addCount() {
        for (int i = 0; i < 5000; i++) {
                count++;
        }
    }

    public long execute() throws InterruptedException {
        //创建两个线程，执行count的累加操作
        Thread threadA = new Thread(() -> {
            addCount();
        });
        Thread threadB = new Thread(() -> {
            addCount();
        });
        threadA.start();
        threadB.start();
        threadA.join();
        threadB.join();
        return count;
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadSafeCache testThread = new ThreadSafeCache();
        long count = testThread.execute();
        System.out.println(count);
    }
}
