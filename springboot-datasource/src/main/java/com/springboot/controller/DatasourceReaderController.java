package com.springboot.controller;

import com.springboot.conf.DatasourceReadProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/11/16 15:33
 * @version: 1.0
 */

/**
 * 一、最常规的做法：通过@Autowired获取DatasourceProperties来获取配置文件属性
 */

@RestController
@Slf4j
public class DatasourceReaderController {

    @Autowired
    private DatasourceReadProperties datasourceReadProperties;

    @RequestMapping("/getReadProperties")
    public String getReadProperties(){
        log.info("getReadProperties");
        return datasourceReadProperties.getUrl();
    }

}
