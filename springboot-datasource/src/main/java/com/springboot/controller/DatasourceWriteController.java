package com.springboot.controller;

import com.springboot.conf.DatasourceWriteProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/11/16 16:38
 * @version: 1.0
 */

@RestController
@Slf4j
public class DatasourceWriteController {

    @Autowired
    private DatasourceWriteProperties datasourceWriteProperties;

    @RequestMapping("/getWriteProperties")
    public String getWriteProperties(){
        log.info("getWriteProperties");
        return datasourceWriteProperties.getDriverClassName();
    }
}
