package com.mybatiss.controller;

import com.mybatiss.model.User;
import com.mybatiss.service.UserService;
import org.springframework.stereotype.Component;
import util.BeanContext;

import java.util.*;
import java.util.concurrent.*;

/**
 * @Author: CPX
 * @Date: 2021/1/12 15:29
 * @version: 1.0
 */

@Component
public class UserSelect extends RecursiveTask<List<User>> {
    private UserService userService;
    /**
     * list集合格式(内容是UserId)
     * {'100015878','100042254','100045334','100046155','100056171'}
     */
    private List<String> list;
    private List<User> returnList;
    // 每个"小任务"最多执行保存50个数
    private int MAX = 50;
    private int start;
    private int end;

    public UserSelect(List<String> list, int start, int end) {
        this.list = list;
        this.start = start;
        this.end = end;
    }

    public UserSelect() {
    }

    @Override
    protected List<User> compute() {
        this.userService = BeanContext.getBean(UserService.class);
        if (returnList == null) {
            returnList = new ArrayList<>();
        }
        // 当end-start的值小于MAX时候，开始执行
        if ((end - start) < MAX) {
            for (int i = start; i < end; i++) {
                //查询数据库，并合并结果
                String userId = list.get(i);
                User user = userService.findById(userId);
                returnList.add(user);
            }
        } else {
            // 将大任务(大于50条强求)分解成两个小任务
            int middle = (start + end) / 2;
            UserSelect left = new UserSelect(list, start, middle);
            UserSelect right = new UserSelect(list, middle, end);
            // 并行执行两个小任务
            left.fork();
            right.fork();
            List<User> join1 = left.join();
            List<User> join2 = right.join();
            returnList.addAll(join1);
            returnList.addAll(join2);

        }
        return returnList;
    }

    // 创建执行的方法

    public List<User> transform(List<String> list) {
        List<User> retList = new ArrayList<>();
        System.out.println("*****************************程序开始执行*****************************");
        // 创建线程池，包含Runtime.getRuntime().availableProcessors()返回值作为个数的并行线程的ForkJoinPool
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        try {
            // 提交可拆分的Task任务
            UserSelect userSelect = new UserSelect(list, start, list.size());
            ForkJoinTask<List<User>> submit = forkJoinPool.submit(userSelect);
            retList = submit.get();
            //阻塞当前线程直到 ForkJoinPool 中所有的任务都执行结束
            forkJoinPool.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        forkJoinPool.shutdown();
        System.out.println("*****************************程序执行结束*****************************");

        return retList;
    }


}
