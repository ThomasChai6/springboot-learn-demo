package com.lenovo.helloword.restTemplate.service;

import com.lenovo.helloword.restTemplate.conf.RestTempConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import java.net.URL;

/**
 * @Author: CPX
 * @Date: 2020/11/13 15:28
 * @version: 1.0
 */

@Component
public class AysncService {

    @Autowired
    RestTempConf restTempConf;

    /**
     * @Async ：异步加载，调用方法，如果过方法一直在等待，但是服务会立即响应，不会一直等待此方法运行
     * 需在app启动类上加上@EnableAsync注解才行使用
     * RestTemplate restful风格的HttpClient工具类
     */
    @Async
    public void update(String strUrl){
        ClientHttpRequestFactory clientHttpRequestFactory = restTempConf.simpleClientHttpRequestFactory();
        RestTemplate restTemplate = restTempConf.initRestTemplate(clientHttpRequestFactory);
        try {
            URL url = new URL(strUrl);
            Thread.sleep(100l);
            restTemplate.delete(url.toURI());
//            restTemplate.exchange(url.toURI(), HttpMethod.PUT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @Configuration 中的@Bean是为了AnnotationConfigApplicationContext方式获取方法用的，如果通过Autowired
     * 这种方式获取不要bean注入
     */
    public void getBean(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(RestTempConf.class);
        String beanTest = context.getBean("beanTest", String.class);
        System.out.println(beanTest);
    }
}
