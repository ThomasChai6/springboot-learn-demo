package com.lenovo.json;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @Author: CPX
 * @Date: 2020/11/17 11:15
 * @version: 1.0
 */

/**
 * 序列化：在网络间可以传输的数据格式，一般分为两种形式
 * 1、二进制数据格式
 * 2、json字符串（一种文件格式的规范）
 */
@Slf4j
public class JsonSer {

    //对象序列化
    @Test
    public void jsonSer() {
        User user = new User("chai", 21, "男");
        String s = JSON.toJSONString(user);
        System.out.println(s);
//        User user1 = JSON.parseObject(s, User.class);
        JSONObject jsonObject = JSON.parseObject(s);
        System.out.println(jsonObject);

    }

    //json字符串序列化
    @Test
    public void jsonSer2() {
        String str = "{'username':'chai','age':21,'sex':'男'}";
        String s = JSON.toJSONString(str);
        System.out.println(s);
    }

    //json字符串反序列化
    public void desJsonSer() {
        String str = "{'username':'chai','age':21,'sex':'男'}";
        User user = JSON.parseObject(str, User.class);

    }

    @Test
    public void StrToJson() {
        String str = "{\"channel\":\"009000\"," +
                "\"data\":\"{\\\"appPackageName\\\":\\\"com.tencent.wecar\\\",\\\"appVersionCode\\\":180,\\\"appVersionName\\\":\\\"1.2.2.180\\\",\\\"cache\\\":false,\\\"channel\\\":\\\"009000\\\",\\\"destInfo\\\":{\\\"POIId\\\":\\\"合生·麒麟社\\\",\\\"destLat\\\":39.997289,\\\"destLon\\\":116.47738}," +
                "\\\"hide\\\":true,\\\"location\\\":[{\\\"accuracy\\\":0.7427651286125183,\\\"altitude\\\":36.3,\\\"bearing\\\":145.7296600341797,\\\"clientTime\\\":1605183062,\\\"cmt\\\":1605183062172,\\\"coorType\\\":1,\\\"fixmode\\\":1,\\\"gmt\\\":1605183062171,\\\"gpstime\\\":1605183062,\\\"hdop\\\":0,\\\"latitude\\\":40.022523,\\\"loctype\\\":2,\\\"longitude\\\":116.281233,\\\"phonedir\\\":0,\\\"sataliteNum\\\":19,\\\"speed\\\":8.303133010864258,\\\"status\\\":0,\\\"vdop\\\":0},{\\\"accuracy\\\":0.7427651286125183,\\\"altitude\\\":36.3,\\\"bearing\\\":145.8034362792969,\\\"clientTime\\\":1605183063,\\\"cmt\\\":1605183063174,\\\"coorType\\\":1,\\\"fixmode\\\":1,\\\"gmt\\\":1605183063172,\\\"gpstime\\\":1605183063,\\\"hdop\\\":0,\\\"latitude\\\":40.022462,\\\"loctype\\\":2,\\\"longitude\\\":116.281287,\\\"phonedir\\\":0,\\\"sataliteNum\\\":18,\\\"speed\\\":8.023275375366211,\\\"status\\\":0,\\\"vdop\\\":0},{\\\"accuracy\\\":0.7427651286125183,\\\"altitude\\\":36.2,\\\"bearing\\\":146.1542358398438,\\\"clientTime\\\":1605183065,\\\"cmt\\\":1605183065173,\\\"coorType\\\":1,\\\"fixmode\\\":1,\\\"gmt\\\":1605183065169,\\\"gpstime\\\":1605183065,\\\"hdop\\\":0,\\\"latitude\\\":40.022354,\\\"loctype\\\":2,\\\"longitude\\\":116.281381,\\\"phonedir\\\":0,\\\"sataliteNum\\\":19,\\\"speed\\\":6.38374137878418,\\\"status\\\":0,\\\"vdop\\\":0}]," +
                "\\\"maxSpeed\\\":0,\\\"mileage\\\":0,\\\"scenceInfo\\\":{\\\"navExtraData\\\":{\\\"currentRouteId\\\":\\\"3601481415079288608\\\",\\\"navSessionId\\\":\\\"6732282407139667355_365712805397518708\\\",\\\"originalRouteId\\\":\\\"3601481415079288608\\\",\\\"routeIds\\\":[\\\"3601481415079288608\\\",\\\"365712805397518708\\\",\\\"7930210960127236860\\\"]}," +
                "\\\"scenceType\\\":1},\\\"sceneExtData\\\":\\\"{\\\\\\\"naviMode\\\\\\\":1,\\\\\\\"eta\\\\\\\":{\\\\\\\"remainCourseDis\\\\\\\":21088,\\\\\\\"remainCourseTime\\\\\\\":1787},\\\\\\\"powerType\\\\\\\":3,\\\\\\\"powerLevel\\\\\\\":1,\\\\\\\"estimatedMileage\\\\\\\":299000,\\\\\\\"powerPercentage\\\\\\\":88,\\\\\\\"power\\\\\\\":100}\\\",\\\"seqId\\\":4830,\\\"sid\\\":\\\"8210cdbc-6352-4a78-9aab-0683da66908e\\\",\\\"status\\\":2,\\\"updateTime\\\":1605183066848,\\\"wecarId\\\":\\\"5ebf509e4e9ebc01218528a5\\\"}\",\"deviceId\":\"864506031111587\",\"eventId\":\"b3269eb627c07162f53bac557ed0d41d473eeac2\",\"eventTime\":1605183066065,\"eventType\":\"SceneGPS\",\"logId\":\"1605183066065393\",\"protoVer\":0,\"userId\":\"009000\",\"wecarId\":\"5ebf509e4e9ebc01218528a5\"}";
        JSONObject jsonObject = JSON.parseObject(str);//此处即为序序列化
        log.info(jsonObject.toString());
        String s = JSON.toJSONString(jsonObject);//此处即为反序列化
        log.info(s);

    }
}
