package com.multidatasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class MybatisMultiApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(MybatisMultiApp.class, args);
        System.out.println( "start multiMybatisDatasource" );
    }
}
