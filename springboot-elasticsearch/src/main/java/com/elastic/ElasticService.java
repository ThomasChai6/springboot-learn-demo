package com.elastic;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;

public class ElasticService {

    /**
     * RestClient的performRequest方法添加索引数据
     * @throws IOException
     */
    public static void getUser() throws IOException {
        RestClient restClient = ElasticsearchClient.getRestClient();
        Response response = restClient.performRequest(new Request("PUT", "/test_index1"));
        String s = EntityUtils.toString(response.getEntity());
        System.out.println(s);
    }

    /**
     * RestClient的performRequest方法获取数据
     * @throws IOException
     */
    public static void putIndex() throws IOException {
        RestClient http = RestClient.builder(new HttpHost("127.0.0.1", 9200, "http")).build();
        Response put = http.performRequest(new Request("GET", "/test_index2"));

        HttpEntity entity = put.getEntity();
        String s = EntityUtils.toString(entity);
        System.out.println(s);
    }

    /**
     * 通过RestHighLevelClient来查询数据，可以进行多条件查询
     * @throws IOException
     */
    public static void searchRuquest() throws IOException {
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost("127.0.0.1", 9200, "http")));
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(QueryBuilders.matchQuery("age", "20"));
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = search.getHits();
        System.out.println("search.toString():" + search.toString());
    }

    public static void indexReq(){

    }

}
