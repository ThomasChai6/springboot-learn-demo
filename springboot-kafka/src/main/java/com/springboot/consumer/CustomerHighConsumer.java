package com.springboot.consumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.TopicPartition;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * @Author: CPX
 * @Date: 2020/12/10 18:08
 * @version: 1.0
 */
public class CustomerHighConsumer {

    public static void main(String[] args) {

        consumerMes();
    }

    public static void consumerMes(){
        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(initProper());
//        kafkaConsumer.subscribe(Arrays.asList("chaitest1","chaitest2","chaitest3"));//消费多个主题的数据
//        kafkaConsumer.subscribe(Collections.singletonList("chaitest1"));//消费一个主题的数据
        //不更换组重新消费，如下写法
        List<TopicPartition> topicPartitions = Arrays.asList(new TopicPartition("chaitest1", 0), new TopicPartition("chaitest1", 1), new TopicPartition("chaitest1", 2));
        kafkaConsumer.assign(topicPartitions);
        kafkaConsumer.seek(new TopicPartition("chaitest1", 0), 0);
        kafkaConsumer.seek(new TopicPartition("chaitest1", 1), 0);
        kafkaConsumer.seek(new TopicPartition("chaitest1", 2), 0);
        while (true){
            ConsumerRecords<String, String> message = kafkaConsumer.poll(1000);//进行拉取数据并消费
            for (ConsumerRecord<String, String> record : message) {
                System.out.println(record.partition()+"---"+record.topic()+"---"+record.offset()+"---"+record.value()+"---"+record.timestamp());
            }

        }
    }



    public static Properties initProper() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "10.122.61.23:9092");
        //设置消费者组id
        //设置是否自动提交offset，在高级api中，会自动管理offset
        props.put("enable.auto.commit", "true");
        //消费数据后多久进行自动提交offset
        props.put("auto.commit.interval.ms", "1000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        //换组重新消费：高级api如果想跟换groupid进行从头读取主题数据，必须设置auto.offset.reset为earliest才行
        props.put(ConsumerConfig.GROUP_ID_CONFIG,"test1111");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        return props;
    }
}
