package com.lenovo.helloword.restTemplate.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;

/**
 * @Author: CPX
 * @Date: 2020/11/13 15:11
 * @version: 1.0
 */

/**
 * @Configuration 跟@Component的作用是一样的，凡是添加@bean注解都能被代理到
 * 所以@Bean一般跟@Configuration 或者@Component使用来注入bean
 */
@Configuration
public class RestTempConf {

    @Bean(name = "restTemplate")
    public RestTemplate initRestTemplate(ClientHttpRequestFactory factory){
        RestTemplate restTemplate = new RestTemplate(factory);
        return restTemplate;
    }

    @Bean(name = "clientHttpRequestFactory")
    public ClientHttpRequestFactory simpleClientHttpRequestFactory(){
        SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        simpleClientHttpRequestFactory.setConnectTimeout(3000);
        simpleClientHttpRequestFactory.setProxy(new Proxy(Proxy.Type.DIRECT, new InetSocketAddress(9200)));
        return simpleClientHttpRequestFactory;
    }

    @Bean(name = "beanTest")
    public String beanTest(){
        System.out.println("beanTest");
        return "beanTest";
    }
}
