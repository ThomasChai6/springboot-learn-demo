package com.thread.basicthread;

import com.sun.org.apache.bcel.internal.generic.NEW;

/**
 * Thread是java.lang包下的
 * @Author: CPX
 * @Date: 2021/1/6 17:03
 * @version: 1.0
 */
public class BasicThread extends Thread{

    @Override
    public void run() {
        System.out.println("basicThread running");
    }
}

class ThreadRun{
    public static void main(String[] args) {
        Thread thread = new Thread(new BasicThread());
        thread.start();

    }
}