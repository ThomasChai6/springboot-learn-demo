package com.springboot.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: CPX
 * @Date: 2020/11/16 17:49
 * @version: 1.0
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "spring.datasource.test")
public class MysqlProperties {
    private String url;
    private String username;
    private String password;
    private String driverClassName;
}
