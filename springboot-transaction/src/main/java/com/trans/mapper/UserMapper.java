package com.trans.mapper;

import com.trans.model.UserEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/12/3 17:17
 * @version: 1.0
 */

@Mapper
public interface UserMapper {

    @Select("select * from t_user")
    public List<UserEntity> findUser();

    @Insert("INSERT INTO t_user(id,name,age,des)VALUES(#{id},#{name},#{age},#{des})")
    public int insertUser(UserEntity userEntity);
}
