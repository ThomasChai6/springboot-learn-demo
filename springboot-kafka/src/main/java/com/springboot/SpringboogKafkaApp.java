package com.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: CPX
 * @Date: 2020/12/16 10:26
 * @version: 1.0
 */

@SpringBootApplication
public class SpringboogKafkaApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringboogKafkaApp.class,args);
        System.out.println("start springBoot kafka");
        String arg0 = args[0];
        String arg1 = args[1];
        System.out.println("arg0:"+arg0+"---arg1:"+arg1);
    }
}
