package com.springboot.conf;

import com.springboot.conf.MysqlProperties;
import com.springboot.conf.TidbProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * @Author: CPX
 * @Date: 2020/11/16 17:39
 * @version: 1.0
 */

@Component
public class DatasourceConf {

    @Autowired
    private MysqlProperties mysqlProperties;

    @Autowired
    private TidbProperties tidbProperties;

    public DataSource mysqlDataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(mysqlProperties.getDriverClassName());
        dataSource.setUrl(mysqlProperties.getUrl());
        dataSource.setUsername(mysqlProperties.getUsername());
        dataSource.setPassword(mysqlProperties.getPassword());
        return dataSource;
    }

    public DataSource tidbDataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(tidbProperties.getUrl());
        dataSource.setUsername(tidbProperties.getUsername());
        dataSource.setPassword(tidbProperties.getPassword());
        dataSource.setDriverClassName(tidbProperties.getDriverClassName());
        return dataSource;
    }

    @Bean
    @Qualifier("mysqlJdbcTemplate")
    public JdbcTemplate mysqlJdbcTemplate(){
        return new JdbcTemplate(mysqlDataSource());
    }

    @Bean
    @Qualifier("tidbJdbcTemplate")
    public JdbcTemplate tidbJdbcTemplate(){
        return new JdbcTemplate(tidbDataSource());
    }



}
