package com.mybatiss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: CPX
 * @Date: 2020/11/19 10:27
 * @version: 1.0
 * @MapperScan 的使用
 * 扫描一个包：@MapperScan("com.kfit.mapper")
 * 扫描多个包：@MapperScan({"com.mybatiss.MapperScanMapper","com.mybatiss.mapper"})
 * 或者@MapperScan("com.kfit.mapper*")
 */
@SpringBootApplication
//@MapperScan({"com.mybatiss.MapperScanMapper","com.mybatiss.mapper"})
public class MybatisApplicationRun {

    public static void main(String[] args) {
        SpringApplication.run(MybatisApplicationRun.class, args);
        System.out.println("start mybatis");
    }
}
