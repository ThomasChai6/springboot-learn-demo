package com.mybatiss.MapperScanMapper;

import com.mybatiss.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/11/19 14:03
 * @version: 1.0
 * 在项目启动类App.run中加入@MapperScan的包扫描，省去@Mapper的繁琐加入
 * @MapperScan: 凡是被扫描的接口都会自动生成实现类，而不用添加@Mapper注解
 *
 */
@Mapper
public interface UserMapperScan {
    @Select("SELECT * FROM t_user u WHERE u.id = #{id}")
    public User findById(String id);

    /**
     * @param id
     * @param name
     * @return
     * Mapper层方法如果是多参数的话，则需要家@Param来指定，不然框架找不到#{}对应的哪一个参数，一个参数则不需要
     */
    @Select("SELECT * FROM t_user u WHERE u.id = #{id} AND u.name=#{name}")
    public User fineByIdAndName(@Param("id") String id,@Param("name") String name);

    @Select("select * from t_user")
    public List<User> findUsers();

    /**
     * @param id
     * @return
     * 查询出来的id和Userbean中的id不匹配，就需要使用@Result来指定
     * column:数据库字段   property:User实体类字段
     */
    @Select({"SELECT u.name 'name',u.age 'age' FROM t_user u WHERE u.id = #{id}"})
    @Results({
            @Result(column = "name",property = "userName"),
            @Result(column = "age",property = "age")
    })
    public List<User> findUsersById(String id);

    /**
     *
     * @param user
     * @return
     * #{}内的名称必须与UserBean的属性值一致
     * Mapper层方法如果是多参数的话，则需要家@Param来指定，不然框架找不到#{}对应的哪一个参数，一个参数则不需要
     */
    @Insert("INSERT INTO t_user(id,name,age,des)VALUES(#{id},#{userName},#{age},#{des})")
    public int insertUser(User user);
    //    @Update("UPDATE user SET user_name = #{username} where id = #{id}")
    @Update("UPDATE t_user u SET u.des = #{des} WHERE u.id = #{id}")
    public void updateUser(@Param("des") String des,@Param("id") int id);
}
