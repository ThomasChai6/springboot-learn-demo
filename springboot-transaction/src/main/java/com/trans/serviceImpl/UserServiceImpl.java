package com.trans.serviceImpl;

import com.trans.mapper.UserMapper;
import com.trans.model.UserEntity;
import com.trans.service.UserService;
import org.apache.ibatis.annotations.Arg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/12/3 17:38
 * @version: 1.0
 * @Service 要加到实现类上
 */

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserMapper userMapper;

    public List<UserEntity> findUsers() {
        return userMapper.findUser();
    }

    public int insertUser(UserEntity userEntity) {
        return userMapper.insertUser(userEntity);
    }
}
