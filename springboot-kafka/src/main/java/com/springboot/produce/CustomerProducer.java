package com.springboot.produce;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Future;

/**
 * @Author: CPX
 * @Date: 2020/12/10 15:48
 * @version: 1.0
 */

@Slf4j
public class CustomerProducer {

    private static Logger logger = LoggerFactory.getLogger(CustomerProducer.class);

    public static void main(String[] args) {

//        produceSend();
        callbackProduceSend();
    }

    //不使用回调函数发送数据
    public static void produceSend() {
        Properties properties = initPro();
        Producer<String, String> producer = new KafkaProducer<>(properties);
        //构建生产者对象
        KafkaProducer<Object, Object> produce = new KafkaProducer<>(properties);
        for (int i = 0; i < 5; i++) {
            ProducerRecord<Object, Object> producerRecord = new ProducerRecord<>("chaitest", Integer.toString(i));
            produce.send(producerRecord);
        }
        produce.close();
    }

    //使用回调函数发送数据
    public static void callbackProduceSend() {
        Properties properties = initPro();
        KafkaProducer<Object, Object> kafkaProducer = new KafkaProducer<>(properties);

        for (int i = 0; i < 9; i++) {
            kafkaProducer.send(new ProducerRecord<>("chaitest1", Integer.toString(i)), new Callback() {
                @Override
                public void onCompletion(RecordMetadata metadata, Exception exception) {
                    long offset = metadata.offset();
                    String topic = metadata.topic();
                    int partition = metadata.partition();
                    System.out.println(metadata.partition()+"---"+metadata.offset());


                }
            });
        }
        kafkaProducer.close();


    }

    public static Properties initPro() {
        Properties props = new Properties();
//        ./kafka-console-consumer.sh --bootstrap-server kafka-test-1:9092 --from-beginning --topic chltest --consumer-property group.id=test-consumer-group-1
        //命令行--bootstrap-server 的设置（offset不存储在zookeeper中）
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "10.122.61.23:9092");
        //应答级别，设置acks方式
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        //失败后重试次数
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        //读取批量数据的大小
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
        props.setProperty(ProducerConfig.PARTITIONER_CLASS_CONFIG, "com.springboot.produce.CustomerPartition");
        //设置序列化
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        List<String> list = new ArrayList<>();
        list.add("com.springboot.intercept.CountIntercept");
        list.add("com.springboot.intercept.TimeIntercept");
        props.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, list);
        return props;
    }

}
