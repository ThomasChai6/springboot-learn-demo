package com.elastic;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * 思想一、通过构造函数和静态方法初始化客户端工具类
 */
public class ElasticsearchClient {

    private static HttpHost httpHost = null;

    public ElasticsearchClient(HttpHost httpHost) {
        this.httpHost = httpHost;
    }

    /**
     * @return
     * 初始化RestHighLevelClient
     */
    public static RestHighLevelClient elasticClient() {
        RestHighLevelClient restHighLevelClient = null;
        if (httpHost == null) {
            httpHost = new HttpHost("127.0.0.1", 9200, "http");
        }
        restHighLevelClient = new RestHighLevelClient(RestClient.builder(httpHost));
        return restHighLevelClient;
    }

    /**
     * @return
     * 初始化RestClient
     */
    public static RestClient getRestClient() {
        RestClient restClient = null;
        if (httpHost == null) {
            httpHost = new HttpHost("127.0.0.1", 9200, "http");
        }
        restClient = RestClient.builder(httpHost).build();
        return restClient;
    }

}
