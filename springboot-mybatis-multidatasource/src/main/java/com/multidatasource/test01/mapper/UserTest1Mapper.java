package com.multidatasource.test01.mapper;

import com.multidatasource.entity.UserEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/12/4 10:50
 * @version: 1.0
 */
@Mapper
public interface UserTest1Mapper {
    @Select("select * from t_user")
    public List<UserEntity> findUser();

    @Insert("INSERT INTO t_user(id,name,age,des)VALUES(#{id},#{name},#{age},#{des})")
    public int insertUser(UserEntity user);
}
