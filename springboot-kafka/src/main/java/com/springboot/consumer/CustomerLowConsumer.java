package com.springboot.consumer;

import kafka.api.FetchRequest;
import kafka.api.FetchRequestBuilder;
import kafka.api.PartitionOffsetRequestInfo;
import kafka.cluster.BrokerEndPoint;
import kafka.common.TopicAndPartition;
import kafka.javaapi.*;
import kafka.javaapi.consumer.SimpleConsumer;
import kafka.javaapi.message.ByteBufferMessageSet;
import kafka.message.MessageAndOffset;

import java.nio.ByteBuffer;
import java.util.*;

/**
 * @Author: CPX
 * @Date: 2020/12/11 10:50
 * @version: 1.0
 */
public class CustomerLowConsumer {

    static List<String> brokers = null;
    static int port = 0;
    static int soTimeout = 0;
    static int bufferSize = 0;
//    static String topic = "iotstudio-message-test01";
    static String topic = "chaitest1";
    static int partition = 0;

    //静态代码块初始化属性
    static {
        brokers = (brokers == null) ? new ArrayList<>() : brokers;
        /*brokers.add("sltf4ax9yjz.novalocal");
        brokers.add("sltlrdwgty2.novalocal");
        brokers.add("sltf4ax9yjz.novalocal");*/
        brokers.add("10.122.61.23");
        brokers.add("10.122.61.228");
        brokers.add("10.122.61.229");

        port = (port == 0) ? 9092 : port;
        soTimeout = (soTimeout == 0) ? 1000 : soTimeout;
        bufferSize = (bufferSize == 0) ? 4*1024 : bufferSize;
    }

    public static void main(String[] args) {
       getData();
    }

    //找分区leader
    public static BrokerEndPoint findLeader(List<String> brokers, int port, int soTimeout, int bufferSize, String clientId, String topic) {
        for (String broker : brokers) {

            //创建获取分区leader的消费者对象
            SimpleConsumer simpleConsumer = new SimpleConsumer(broker, port, soTimeout, bufferSize, clientId);
            //获取主题元数据返回值response
            //获取主题元数据请求request
            TopicMetadataRequest topicMetadataRequest = new TopicMetadataRequest(Collections.singletonList(topic));
            //获取主题元数据的响应信息
            TopicMetadataResponse topicMetadataResponse = simpleConsumer.send(topicMetadataRequest);
            //解析元数据返回值
            List<TopicMetadata> topicMetadata = topicMetadataResponse.topicsMetadata();
            //遍历主题元数据信息
            for (TopicMetadata topicMetadatum : topicMetadata) {
                System.out.println(topicMetadatum.toString());
                //当前主题里面有多个分区，所以是个集合
                List<PartitionMetadata> partitionMetadata = topicMetadatum.partitionsMetadata();
                for (PartitionMetadata partitionMetadatum : partitionMetadata) {
                    System.out.println("leader:"+partitionMetadatum.partitionId());
                    //当前以查找分区为0的leader
                    if(0 == partitionMetadatum.partitionId()){
                        System.out.println("----------");
                        return partitionMetadatum.leader();
                    }
                }
            }
        }
        return null;
    }

    //获取主题数据
    public static void getData() {
        //获取leader
        BrokerEndPoint brokerEndPoint = findLeader(brokers, port, soTimeout, bufferSize, "getLeader", topic);
        if(brokerEndPoint == null){
            return;
        }
        //通过leader获取消费者对象
        SimpleConsumer getData = new SimpleConsumer(brokerEndPoint.host(), port, soTimeout, bufferSize, "getData");

        //获取最新的offset
        long lastOffset = getLastOffset(getData, topic, 0, 0L, "getlastoffset");

        FetchRequest fetchRequest = new FetchRequestBuilder().addFetch(topic, partition, lastOffset, bufferSize).build();
        //拉取数据
        FetchResponse fetchResponse = getData.fetch(fetchRequest);
        ByteBufferMessageSet messageAndOffsets = fetchResponse.messageSet(topic, partition);
        for (MessageAndOffset messageAndOffset : messageAndOffsets) {
            long offset = messageAndOffset.offset();
            ByteBuffer payload = messageAndOffset.message().payload();
            byte[] bytes = new byte[payload.limit()];
            String s = new String(bytes);
            System.out.println(offset+"-----"+s);
        }

    };

    //获取最新的offset
    public static long getLastOffset(SimpleConsumer consumer, String topic, int
            partition, long whichTime, String clientName) {
        TopicAndPartition topicAndPartition = new TopicAndPartition(topic, partition);
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo = new
                HashMap<TopicAndPartition, PartitionOffsetRequestInfo>();
        requestInfo.put(topicAndPartition, new PartitionOffsetRequestInfo(whichTime,
                1));
        OffsetRequest offsetRequest = new OffsetRequest(requestInfo, kafka.api.OffsetRequest.CurrentVersion(), clientName);
        OffsetResponse response = consumer.getOffsetsBefore(offsetRequest);
        if (response.hasError()) {
            System.out.println("Error fetching data Offset Data the Broker.Reason:"
                    + response.errorCode(topic, partition));
            return 0;
        }
        long[] offsets = response.offsets(topic, partition);
        return offsets[0];
    }

}
