package com.trans.service;

import com.trans.model.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/12/3 17:38
 * @version: 1.0
 */

public interface UserService {

    public List<UserEntity> findUsers();

    public int insertUser(UserEntity userEntity);
}
