package com.mybatiss.controller;

import com.mybatiss.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2021/1/12 17:47
 * @version: 1.0
 */
@RestController
public class TransForm {

  /*  @Autowired
    UserSelect userSelect;*/

    @RequestMapping("/getUserList")
    public List<User> getUserList(@RequestParam(value = "userIds",required = false) List<String> userIds){
        UserSelect userSelect = new UserSelect();
        List<User> transform = userSelect.transform(userIds);
        System.out.println("transform:"+transform);
        return transform;
    }
}
