package com.lenovo.helloword.restTemplate.controller;

import com.lenovo.helloword.restTemplate.service.AysncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.net.www.http.HttpClient;

import java.net.URL;

/**
 * @Author: CPX
 * @Date: 2020/11/13 15:48
 * @version: 1.0
 */

/**
 * @PathVariable 为spring3.0的新功能，用于获取请求路径中的占位符
 */
@RestController
public class AysncController {

    @Autowired
    AysncService aysncService;

    @RequestMapping("/{str}")
    public void update(@PathVariable String url){
        aysncService.update(url);
    }

    @RequestMapping("/")
    public String helloWord(){

        System.out.println("this is helloWord");
        return "hello word";
    }

    @RequestMapping("/getBean")
    public String getBean(){
        aysncService.getBean();
        return "getBean";
    }

    public void test(){
//        HttpClient httpClient = new HttpClient(new URL(""))
    }


}
