package com.mr.email;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.BZip2Codec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.CombineTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

public class EmailDriver {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
/*
        Configuration conf1 = new Configuration();
        FileSystem hdfs1 = FileSystem.get(conf1);
        FileStatus[] fileStatuses = hdfs1.globStatus(new Path("D://test"));
        String[] remainingArgs = new GenericOptionsParser(conf1, args).getRemainingArgs();
            System.out.println("remainingArgs:"+remainingArgs);
            System.out.println("remainingArgs[0]:"+remainingArgs[0]);
            System.out.println("remainingArgs[1]:"+remainingArgs[1]);
        for (FileStatus fileStatus : fileStatuses) {
            Path path = fileStatus.getPath();
            String owner = fileStatus.getOwner();
            System.out.println("path:"+path);
            System.out.println("owner:"+owner);
        }
*/

        Configuration conf = new Configuration();
        FileSystem hdfs = FileSystem.get(conf);
        Job job = Job.getInstance(conf,"emailJob");
        job.setJarByClass(EmailDriver.class);
        job.setMapperClass(EmailMapper.class);
        job.setReducerClass(EmailReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);

        LazyOutputFormat.setOutputFormatClass(job, TextOutputFormat.class);
        FileOutputFormat.setOutputCompressorClass(job, BZip2Codec.class);

        //使用CombineTextInputFormat进行合并小文件
        job.setInputFormatClass(CombineTextInputFormat.class);
        CombineTextInputFormat.setMaxInputSplitSize(job, 4);

        FileInputFormat.setInputPaths(job,"D:\\InputPaths\\email\\email.txt");
        FileInputFormat.addInputPath(job,new Path("D:\\InputPaths\\email\\email2.txt"));
        String outputpath = "D:\\OutputPath\\email";
        Path path = new Path(outputpath);
        boolean exists = hdfs.exists(path);
        if(exists){
            hdfs.delete(path,true);
        }
        FileOutputFormat.setOutputPath(job,new Path("D:\\OutputPath\\email\\"));
        job.submit();
        //yarn的runjar方式运行
        job.waitForCompletion(true);





    }
}
