package com.thread.concurrent.AQS.locks;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;

import java.util.concurrent.Semaphore;

/**
 *1、本例主要实现RedissonLock+Redis实现分布式锁
 * @Author: CPX
 * @Date: 2021/1/14 14:09
 * @version: 1.0
 */
public class RedissonLockDemo {

    public void distributeLock(){
        Config config = new Config();
        ClusterServersConfig clusterServersConfig = config.useClusterServers().setScanInterval(2000);
        clusterServersConfig.
                addNodeAddress("redis://127.0.0.1:7000").
                addNodeAddress("redis://127.0.0.1:7001").
                addNodeAddress("redis://127.0.0.1:7002");//添加redis节点
        RedissonClient redissonClient = Redisson.create(config);
        RLock redisson = redissonClient.getLock("redisson");
        redisson.lock();//获取分布式锁

        try {
            //编写业务逻辑
            Semaphore semaphore = new Semaphore(10);
            semaphore.tryAcquire();
        }catch (Exception e){
            e.printStackTrace();
        }finally {

        }
        redisson.unlock();//释放分布式锁

    }


}
