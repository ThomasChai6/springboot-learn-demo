package com.springboot.intercept;

import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Headers;

import java.util.Map;

/**
 * @Author: CPX
 * @Date: 2020/12/14 12:56
 * @version: 1.0
 */
public class TimeIntercept implements ProducerInterceptor<String,String> {

    @Override
    public ProducerRecord<String, String> onSend(ProducerRecord<String, String> record) {
        return new ProducerRecord<String, String>(record.topic(), record.partition(), System.currentTimeMillis(), record.key(), record.value());
    }

    /**
     * @param record
     * @return
     *  ProducerRecord中有这些参数，根据需求通过拦截器传入相应参数
     *  private final String topic;
     *  private final Integer partition;
     *  private final Headers headers;
     *  private final K key;
     *  private final V value;
     *  private final Long timestamp;
     */

    //类似于回调函数，成功失败均会调用
    @Override
    public void onAcknowledgement(RecordMetadata metadata, Exception exception) {

    }

    @Override
    public void close() {

    }

    /**
     * 可以将需要的配置项通过此方法存储到map中
     * @param configs
     */
    @Override
    public void configure(Map<String, ?> configs) {

    }
}
