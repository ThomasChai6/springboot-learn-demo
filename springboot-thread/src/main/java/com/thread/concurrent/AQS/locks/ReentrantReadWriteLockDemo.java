package com.thread.concurrent.AQS.locks;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @Author: CPX
 * @Date: 2021/1/14 14:11
 * @version: 1.0
 */
public class ReentrantReadWriteLockDemo {

    private ReentrantReadWriteLock.ReadLock readLock;//读锁是共享锁
    private ReentrantReadWriteLock.WriteLock writeLock;//写锁是独享锁
    private ReentrantReadWriteLock reentrantReadWriteLock;

    //通过构造方式初始化ReentrantReadWriteLock，启动类后只初始化一次
    public ReentrantReadWriteLockDemo() {
        if (reentrantReadWriteLock == null) {
            reentrantReadWriteLock = new ReentrantReadWriteLock();
        }
    }

    public void readLock() {
        readLock = reentrantReadWriteLock.readLock();
        readLock.lock();
        try {
            System.out.println("读取锁业务");
        } finally {
            readLock.unlock();
        }
    }

    public void writeLock() {
        writeLock = reentrantReadWriteLock.writeLock();
        writeLock.lock();
        try {
            System.out.println("写入锁业务");
        } finally {
            writeLock.unlock();
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                new ReentrantReadWriteLockDemo().readLock();
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                new ReentrantReadWriteLockDemo().writeLock();
            }
        });

        thread1.start();
        thread2.start();
    }

}
