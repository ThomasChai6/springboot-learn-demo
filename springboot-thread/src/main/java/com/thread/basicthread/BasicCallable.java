package com.thread.basicthread;

import java.text.SimpleDateFormat;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 1、Callable接口可以返回执行结果，结合Future的FutureTask创建Thread进行创建线程
 *  实现Callable要重写call()接口，该方法里写线程执行完的返回结果
 * 2、FutureTask.get()方法可以获取到线程执行结果
 *
 * @Author: CPX
 * @Date: 2021/1/8 16:20
 * @version: 1.0
 */
public class BasicCallable implements Callable<String> {
    @Override
    public String call() throws Exception {
        return "线程完成";
    }

}

class CallableRun{
    public static void main(String[] args) {
        FutureTask<String> stringFutureTask = new FutureTask<>(new BasicCallable());
        Thread thread = new Thread(stringFutureTask);
        thread.start();
        try {
            String s = stringFutureTask.get();
            System.out.println(s);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
