package com.trans;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: CPX
 * @Date: 2020/12/3 17:27
 * @version: 1.0
 */

@SpringBootApplication
@Slf4j
public class TransApp {

    private final Logger logger = LoggerFactory.getLogger(TransApp.class);

    public static void main(String[] args) {
        SpringApplication.run(TransApp.class, args);
        log.info("start transaction");
    }
}
