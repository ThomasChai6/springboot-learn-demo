package com.springboot.service.userservice;

import com.springboot.dao.userdao.UserDao;
import org.springframework.stereotype.Service;

/**
 * @Author: CPX
 * @Date: 2020/11/16 18:06
 * @version: 1.0
 */
@Service
public class UserService {

    UserDao userDao = new UserDao();
    public void queryUser(){
        userDao.queryUser();

    }

}
