package com.thread.threadsafe;

import io.swagger.models.auth.In;

/**
 * 在没有使用ThreadLocal的时候，全局变量count存在线程安全问题
 * 注意：如果调用线程一致不终止，则本地变量会一直存放在调用线程的threadLocals成员变量中，所
 * 以，如果不需要使用本地变量时，可以通过调用ThreadLocal的remove()方法，将本地变量从当前线
 * 程的threadLocals成员变量中删除，以免出现内存溢出的问题。
 *
 * @Author: CPX
 * @Date: 2021/1/11 17:23
 * @version: 1.0
 */
public class ThreadLocalTest {

    private static ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>(){
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };
    private static Integer count = 0;

    /**
     * 不使用ThreadLocal 全局变量count存在线程安全问题
     */
    static class TestNoThreadLocal {

        public static void test() {
            Thread thread1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 1000; i++) {
                        count++;
                    }
                    System.out.println("线程A的值：" + count);
                }
            });
            Thread thread2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 1000; i++) {
                        count++;
                    }
                    System.out.println("线程B的值：" + count);
                }
            });

            thread1.start();
            thread2.start();
        }
    }

    /**
     * 使用ThreadLocal，所有的操作都是本地内存执行，各个线程之间不互相影响，线程安全
     */
    static class TestWithThreadLocal {
        public static void test(){
            Thread thread1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 1000; i++) {
                        threadLocal.set(threadLocal.get() + 1);
                    }
                    System.out.println("线程A的值:"+threadLocal.get());
                    count = 0;
                }
            });

            Thread thread2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 1000; i++) {
                        threadLocal.set(threadLocal.get() + 1);
                    }
                    System.out.println("线程B的值:"+threadLocal.get());
                    count = 0;
                }
            });
            thread1.start();
            thread2.start();
        }
    }

    public static void main(String[] args) {
     /*   for (int i = 0; i < 10; i++) {
            TestNoThreadLocal.test();
        }*/
        for (int i = 0; i <10 ; i++) {
            TestWithThreadLocal.test();
        }
    }


}
