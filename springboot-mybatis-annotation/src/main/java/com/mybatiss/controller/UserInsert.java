package com.mybatiss.controller;

import com.mybatiss.model.User;
import com.mybatiss.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import util.MathUtils;

import java.util.List;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: CPX
 * @Date: 2021/1/12 11:00
 * @version: 1.0
 */

@RestController
public class UserInsert {

    @Autowired
    UserService userService;

    @RequestMapping("/usersInsert")
    public void usersInsert(){
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(10, 20, 1000, TimeUnit.MILLISECONDS, new SynchronousQueue<>());
        for (int i = 0; i < 10; i++) {
            threadPoolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 3000; i++) {
                        long time = Long.parseLong(MathUtils.randomDigitNumber(9));
                        User user = new User();
                        user.setAge(i);
                        user.setDes("des-"+i);
                        user.setId(time);
                        user.setUserName("li-"+i);
                        userService.insertUser(user);

                    }
                }
            });
        }
        threadPoolExecutor.shutdown();

    }

    @RequestMapping(path = "/getList",method = RequestMethod.POST)
    public List<String> getList(@RequestParam(value = "userIds",required = false) List<String> userIds){
        System.out.println("userIds:"+userIds);
        return userIds;
    }

    public static void main(String[] args) {
        new UserInsert().usersInsert();

    }

}
