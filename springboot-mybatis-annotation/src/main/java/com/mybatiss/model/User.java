package com.mybatiss.model;

import lombok.Data;

/**
 * @Author: CPX
 * @Date: 2020/11/19 10:10
 * @version: 1.0
 */
@Data
public class User {
    private Long id;
    private String userName;
    private Integer age;
    private String des;
}
