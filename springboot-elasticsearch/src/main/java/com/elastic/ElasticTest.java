package com.elastic;

import org.apache.http.HttpHost;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;

/**
 * @Author: CPX
 * @Date: 2020/11/5 13:39
 * @version: 1.0
 */
public class ElasticTest {

    private RestHighLevelClient client;
    private RestClient restClient;
    private Logger logger = Logger.getLogger(ElasticTest.class);


    @Before
    public void initClient() {
        if (client == null) {
            logger.info("initClient begin");
            client = new RestHighLevelClient(RestClient.builder(new HttpHost("127.0.0.1", 9200, "http")));
        }

        if (restClient == null) {
            restClient = ElasticsearchClient.getRestClient();
        }
    }

    @Test
    public void createIndex() {
        try {
            CreateIndexRequest index_test1 = new CreateIndexRequest("index_test1");
            CreateIndexResponse createIndexResponse = client.indices().create(index_test1, RequestOptions.DEFAULT);
            logger.info(createIndexResponse.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void insertDocument() {
        try {
            //source()参数必须为偶数
            for (int i = 0; i < 10000; i++) {
                logger.info(i);
                    IndexRequest indexRequest = new IndexRequest("scroll_index", "_doc", i+"").source("user", "kimchy",
                            "postDate", new Date(),
                            "message", "trying out Elasticsearch", "name", "chai"+i);
                    IndexResponse index = client.index(indexRequest, RequestOptions.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void existIndex() {
        try {
            GetIndexRequest index_test1 = new GetIndexRequest("index_test2");
            boolean exists = client.indices().exists(index_test1, RequestOptions.DEFAULT);
            logger.info(exists);

            GetIndexResponse getIndexResponse = client.indices().get(index_test1, RequestOptions.DEFAULT);
            logger.info(getIndexResponse.toString());
        } catch (IOException e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        } finally {
            System.exit(1);
        }

    }

    @Test
    public void deleteIndex() {
        try {
            DeleteIndexRequest index_test2 = new DeleteIndexRequest("index_test2");
            AcknowledgedResponse delete = client.indices().delete(index_test2, RequestOptions.DEFAULT);
            logger.info(delete.isAcknowledged());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    SearchRequest:查询请求
    sourcebuilder：请求构建
    QueryBuilders:构建查询条件
    */
    @Test
    public void getIndex() {
        try {
//            使用RestHighLevelClient实现
            SearchRequest searchRequest = new SearchRequest("index_test1");
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchRequest.source(searchSourceBuilder);
            SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
            logger.info(search.toString());
//            使用RestClient实现
            Request request = new Request("GET", "index_test1");
            Response response = restClient.performRequest(request);
            logger.info(EntityUtils.toString(response.getEntity()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

  /*  @Test
    public void getIndex2() {
        try {
            SearchRequest searchRequest = new SearchRequest("index_test1");
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            SearchSourceBuilder query = searchSourceBuilder.query(QueryBuilders.matchAllQuery());
            searchRequest.source(searchSourceBuilder);
//            SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
            Cancellable cancellable = client.searchAsync(searchRequest, RequestOptions.DEFAULT, new ActionListener<SearchResponse>() {
                @Override
                public void onResponse(SearchResponse searchResponse) {
                    logger.info("searchResponse:"+searchResponse.toString());
                }

                @Override
                public void onFailure(Exception e) {
                    logger.info("fail------------");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Test
    public void searchScroll() throws IOException {
        SearchRequest searchRequest = new SearchRequest("scroll_index");
//        SearchRequest searchRequest = new SearchRequest(".monitoring-es-6-2020.11.03");
        Scroll scroll = new Scroll(new TimeValue(2l));
        searchRequest.scroll(scroll);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(8000);//设置一次查询3000条,一万条数据要查询四次
        searchRequest.source(searchSourceBuilder);

        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);
        SearchHit[] hits = search.getHits().getHits();
        logger.info("hits.length1:"+hits.length);
        String scrollId = search.getScrollId();

        while (hits != null && hits.length>0){
            if(scroll==null){
                break;
            }
            SearchScrollRequest searchScrollRequest = new SearchScrollRequest();
            searchScrollRequest.scroll(scroll);
            searchScrollRequest.scrollId(scrollId);
            searchScrollRequest.scroll(new Scroll(new TimeValue(2l)));

            SearchResponse searchResponse = client.searchScroll(searchScrollRequest, RequestOptions.DEFAULT);
            hits = searchResponse.getHits().getHits();
            scrollId = searchResponse.getScrollId();
//            logger.info("scrollid:"+scrollId);
            logger.info("hits.length:"+hits.length);

        }

        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);

    }


}
