package com.mybatiss.controller;

import com.mybatiss.mapper.UserMapper;
import com.mybatiss.model.User;
import com.mybatiss.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/11/19 10:22
 * @version: 1.0
 */
@RestController
public class UserController {

    @Autowired
    UserService userService;


    @RequestMapping("/findById")
    public User findById() {
        String id = "1";
        return userService.findById(id);
    }

    @RequestMapping("fineByIdAndName")
    public User fineByIdAndName(String id, String name) {
        return userService.fineByIdAndName(id, name);
    }

    @RequestMapping("/findUsers")
    public List<User> findUsers() {
        return userService.findUsers();
    }

    @RequestMapping("/findUsersById")
    public List<User> findUsersById(){
        String id = "1";
        System.out.println(userService.findUsersById(id));
        return userService.findUsersById(id);
    }

    @RequestMapping(value = "/insertUser")
    public int insertUser(){
        long time = new Date().getTime()/1000;
        User user = new User();
        user.setAge(11);
        user.setDes("aaa");
        user.setId(time);
        user.setUserName("li");
        return userService.insertUser(user);
    }

    @RequestMapping("/updateUser")
    public void updateUser(){
        userService.updateUser("修改了", 2);
    }

    @RequestMapping("/findUser2")
    public List<User> findUsers2(){
        return userService.findUsers2();
    }
}
