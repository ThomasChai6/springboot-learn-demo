package com.trans.model;

import lombok.Data;

/**
 * @Author: CPX
 * @Date: 2020/12/3 17:16
 * @version: 1.0
 */
@Data
public class UserEntity {

    private Integer id;
    private String name;
    private Integer age;
    private String des;
}
