package com.multidatasource.test02.service;

import com.multidatasource.entity.UserEntity;
import com.multidatasource.test02.mapper.UserTest2Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: CPX
 * @Date: 2020/12/4 14:03
 * @version: 1.0
 */

@Service
public class UserTest2Service {

    @Autowired
    UserTest2Mapper userTest2Mapper;

    @Transactional(transactionManager = "test2TransactionManager")
    public int insertUser2(UserEntity userEntity){
        return userTest2Mapper.insertUser2(userEntity);
    }
}
