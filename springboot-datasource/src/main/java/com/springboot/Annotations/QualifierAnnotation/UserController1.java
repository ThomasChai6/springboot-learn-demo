package com.springboot.Annotations.QualifierAnnotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: CPX
 * @Date: 2020/11/16 17:06
 * @version: 1.0
 */

/**
 * @Qualifier 的使用
 * 一、当两个类同时实现了两个一个接口，在@Autowired注入bean时不能确定指定哪个实现类，所以需要@Qualifier来指定
 * 或者在实现类上加上@Primary，默认使用此实现类
 */
@RestController
public class UserController1 {

    @Qualifier("user1")
    @Autowired
    UserInterface userInterface;

    @RequestMapping("/getUser")
    public String  getUser(){
        return userInterface.getUser();
    }

}
