package com.thread;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.StreamUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * @Author: CPX
 * @Date: 2021/1/8 15:20
 * @version: 1.0
 */
public class Test extends BasicTest{

    @Override
    public void basicTest() {
        super.basicTest();
    }

    public void test2(){
        basicTest();
    }

    public static void main(String[] args) {

        /*if (org.springframework.util.StringUtils.isEmpty("")){
            return;
        }
        if(StringUtils.isBlank("")){
            return;
        }
        new Test().test2();*/


        List<String> list = new ArrayList<>();
        list.add("aaaaa");
        list.add("bbb");
        Stream<String> stream = list.stream().filter(B -> {
            boolean ret = false;
            if (B.length() > 3) {
                ret = true;
            }
            return ret;
        });
        Iterator<String> iterator = stream.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        list.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return false;
            }
        });
    }
}
