package com.thread.basicthread;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.*;

/**
 * @Author: CPX
 * @Date: 2021/1/7 13:27
 * @version: 1.0
 * SimpleDateFormat在高并发下会出现线程安全问题
 * 四种解决办法（除了第四项，其他三项针对所有线程不安全的类或者方法都适用）
 * 1、加synchronized关键字，但是高并发下不建议用，效率低，同一时间只能有一个线程执行
 * 2、加lock关键字，跟synchronized本质一致，都是JVM锁机制进行加锁，高并发下也不建议用
 * 3、通过"，推荐在高并发环境使用，它是将线程副本保存到ThreadLocal中，多个线程之间互不影响
 * 4、使用DateTimeFormatter类，此类是线程安全的，单纯针对事件转换类的解决办法
 */
public class SimpleDataFormatTh {
    //执行总次数
    private static final int EXECUTE_COUNT = 1000;
    //同时运行的线程数量
    private static final int THREAD_COUNT = 20;
    //SimpleDateFormat对象
    private static SimpleDateFormat simpleDateFormat = new
            SimpleDateFormat("yyyy-MM-dd");

    /**
     * synchronized关键字
     *
     * @throws InterruptedException
     */
    public void SimpleDataFormatTest1() throws InterruptedException {
        final Semaphore semaphore = new Semaphore(THREAD_COUNT);
        final CountDownLatch countDownLatch = new CountDownLatch(EXECUTE_COUNT);
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < EXECUTE_COUNT; i++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    try {
                        simpleDateFormat.parse("2020-01-01");
                    } catch (ParseException e) {
                        System.out.println("线程：" +
                                Thread.currentThread().getName() + " 格式化日期失败");
                        e.printStackTrace();
                        System.exit(1);
                    } catch (NumberFormatException e) {
                        System.out.println("线程：" +
                                Thread.currentThread().getName() + " 格式化日期失败");
                        e.printStackTrace();
                        System.exit(1);
                    }
                    semaphore.release();
                } catch (InterruptedException e) {
                    System.out.println("信号量发生错误");
                    e.printStackTrace();
                    System.exit(1);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();
        System.out.println("所有线程格式化日期成功");

    }

    /**
     * 通过ThreadLocal保证线程安全，将每个线程使用的SimpleDateFormat副本保存在ThreadLocal中，各个线程在使
     * 用时互不干扰，从而解决了线程安全问题
     * new ThreadLocal<DateFormat>()：副本返回值，通过返回值进行调用方法
     * Semaphore：限制同时访问资源的线程数
     * CountDownLatch：当CountDownLatch不为0是，当前所有线程会进入一个阻塞队列中
     */
    static class ThreadLocalTest {

        public ThreadLocal<DateFormat> threadLocal = new ThreadLocal<DateFormat>() {
            @Override
            protected DateFormat initialValue() {
                return new SimpleDateFormat("yyyy-MM-dd");
            }
        };

        private final int threadCount = 20;
        private final int executeCount = 2000;
        Semaphore semaphore = new Semaphore(threadCount);
        CountDownLatch countDownLatch = new CountDownLatch(executeCount);

        public void ThreadLocalTest() {
            ExecutorService executorService = Executors.newCachedThreadPool();
            for (int i = 0; i < executeCount; i++) {
                System.out.println(i);
                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            semaphore.acquire();
                            threadLocal.get().parse("2020-10-10");
                        } catch (ParseException e) {
                            System.out.println("日期转换失败1");
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            System.out.println("日期转换失败2");
                            e.printStackTrace();
                        }
                        countDownLatch.countDown();
                    }
                });
            }
            try {
                System.out.println("所有日期转换成功");
                countDownLatch.await();
                executorService.shutdown();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public static void main(String[] args) {
        new ThreadLocalTest().ThreadLocalTest();
    }
}
