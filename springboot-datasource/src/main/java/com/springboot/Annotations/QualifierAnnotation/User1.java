package com.springboot.Annotations.QualifierAnnotation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * @Author: CPX
 * @Date: 2020/11/16 17:04
 * @version: 1.0
 */

@Slf4j
@Component("user1")
@Primary
public class User1 implements UserInterface{
    @Override
    public String getUser() {
        log.info("user1");
        return "user1";
    }
}
