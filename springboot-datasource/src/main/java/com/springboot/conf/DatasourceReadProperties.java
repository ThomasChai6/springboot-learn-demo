package com.springboot.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: CPX
 * @Date: 2020/11/16 15:30
 * @version: 1.0
 */


/**
 * 一、@ConfigurationProperties 的使用（默认读取application.properties中的）
 * 1、prefix前缀定义了哪些属性绑定到了当前配置类中，比如该例子中spring.datasource开头的属性会被绑定到DatasourceProperties中
 * 2、类字段中必须要有setter方法（@Data实现了set/get方法）
 * 3、根据springboot的宽松绑定规则，类中属性必须与配置文件的名字相对应
 * 4、类本身可以是包私有的
 *
 * 二、本例中@Configuration（跟@Component作用一样）利用springIOC原理，将DatasourceProperties注册到spring上下文中
 */
@Data
@ConfigurationProperties(prefix = "spring.datasource.read",ignoreUnknownFields = false)
@Configuration
public class DatasourceReadProperties {

    private String url;
    private String password;
    private String username;
    private String driverClassName;
    private String type;


}
