package com.multidatasource.test02.mapper;

import com.multidatasource.entity.UserEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: CPX
 * @Date: 2020/12/4 14:00
 * @version: 1.0
 */

@Mapper
public interface UserTest2Mapper {

    @Insert("INSERT INTO t_user(id,name,age,des)VALUES(#{id},#{name},#{age},#{des})")
    public int insertUser2(UserEntity userEntity);
}
