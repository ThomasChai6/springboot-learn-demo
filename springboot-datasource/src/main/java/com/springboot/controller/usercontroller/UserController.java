package com.springboot.controller.usercontroller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.*;

/**
 * @Author: CPX
 * @Date: 2020/11/16 18:03
 * @version: 1.0
 */

/**
 * swagger:类和方法文档描述的插件
 */
@RestController
@Slf4j
@EnableSwagger2
@Api(value = "value",tags = "tags")
public class UserController {

    @Autowired
    @Qualifier("mysqlJdbcTemplate")
    private JdbcTemplate mysqlTemplate;

    @Autowired
    @Qualifier("tidbJdbcTemplate")
    private JdbcTemplate tidbTemplate;

    @RequestMapping("/queryUser")
    @ApiOperation(value = "queryUser value",tags = "queryUser tags")
    public List<Map<String, Object>>  queryUser(){
        List<Map<String, Object>> maps = mysqlTemplate.queryForList("SELECT * FROM t_user");

        for (int i = 0; i < maps.size(); i++) {
            Map<String, Object> user = maps.get(i);
            String name = (String) user.get("name");
            log.info("name:"+name);
        }
        return maps;
    }

    @RequestMapping("/addUser")
    public void insert(){
        try {
            String time = new Date().getTime() + "";
            String sql = "INSERT INTO t_user VALUES ("+time+", 'chai', '2', 'des');";
            mysqlTemplate.execute(sql);
            log.error("success");
        } catch (DataAccessException e) {
            log.error("error");
            e.printStackTrace();
        }
    }

    @RequestMapping("/getTidbData")
    public List<Map<String, Object>> getTidbData(){
        List<Map<String, Object>> maps = tidbTemplate.queryForList("SELECT * FROM Aiot_Thing");
        for (int i = 0; i < maps.size(); i++) {
            Map<String, Object> map = maps.get(i);
            log.info((String) map.get("name"));
        }
        return maps;
    }
}

