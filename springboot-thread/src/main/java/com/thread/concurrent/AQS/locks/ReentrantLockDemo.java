package com.thread.concurrent.AQS.locks;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 1、ReentrantLock:可重入锁，可以循环获取锁
 * 2、ReentrantLock和synchronized的区别
 *      1）两者都是可重入锁，synchronized比较简单，释放锁用手动释放，而ReentrantLock需要自己获取锁和释放锁
 *      2）ReentrantLock可以通过构造方法获取公平锁和非公平锁，但是synchronized只是非公平锁
 *      3）synchronized是java自带的锁关键字，而ReentrantLock是AQS（AbstractQueuedSynchronized）锁中的可重入锁
 * @Author: CPX
 * @Date: 2021/1/14 13:58
 * @version: 1.0
 */
public class ReentrantLockDemo {

    public static void reentrantLockTest(){
        ReentrantLock reentrantLock = new ReentrantLock(true);//创建公平锁
        try {
            //可多次获取锁，然后多次释放
            for (int i = 0; i < 3; i++) {
                reentrantLock.lock();
                System.out.println(i);
            }
            for (int i = 0; i < 3; i++) {
                reentrantLock.unlock();
            }
        }finally {
        }
    }

    public static void main(String[] args) {
        reentrantLockTest();
    }

}
