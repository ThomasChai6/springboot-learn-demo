package com.multidatasource.entity;

import lombok.Data;

/**
 * @Author: CPX
 * @Date: 2020/12/4 10:52
 * @version: 1.0
 */
@Data
public class UserEntity {

    private Integer id;
    private String name;
    private Integer age;
    private String des;
}
