package com.trans.controller;


import com.trans.model.UserEntity;
import com.trans.service.UserService;
import com.trans.serviceImpl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: CPX
 * @Date: 2020/12/3 17:23
 * @version: 1.0
 * @Transactional 事务管理的验证，加了注解后，insertUser报错后就会回滚，不往mysql中添加数据了
 */
@RestController
public class UserController {

    //引入接口而非实现类
    @Autowired
    UserService userService;

    @RequestMapping("/findUsers")
    public List<UserEntity> findUsers(){
        List<UserEntity> user = userService.findUsers();
        return user;
    }

    @RequestMapping("/insertUser")
    @Transactional
    public int insertUser(){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(100);
        userEntity.setName("whoAreYou");
        userEntity.setAge(11);
        userEntity.setDes("trans");
        int i = userService.insertUser(userEntity);
        int a = 1/0;
        return i;
    }
}
