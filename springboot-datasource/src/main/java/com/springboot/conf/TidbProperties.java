package com.springboot.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: CPX
 * @Date: 2020/11/17 17:40
 * @version: 1.0
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "spring.datasource.tidb")
public class TidbProperties {
    private String url;
    private String username;
    private String password;
    private String driverClassName;
}
