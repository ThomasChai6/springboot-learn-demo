package com.springboot.intercept;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Map;

/**
 * @Author: CPX
 * @Date: 2020/12/14 13:09
 * @version: 1.0
 */
public class CountIntercept implements ProducerInterceptor <String,String>{

    private int successCount = 0;
    private int failCount = 0;
    @Override
    public ProducerRecord<String, String> onSend(ProducerRecord<String, String> record) {
        return record;
    }

    @Override
    public void onAcknowledgement(RecordMetadata metadata, Exception exception) {
        if(exception == null){
            successCount++;
        }else {
            failCount++;
        }
    }

    @Override
    public void close() {
        System.out.println("successCount:"+successCount);
        System.out.println("failCount:"+failCount);
    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}
