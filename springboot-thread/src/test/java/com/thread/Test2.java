package com.thread;


import jdk.nashorn.internal.ir.Node;
import org.redisson.api.RFuture;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @Author: CPX
 * @Date: 2021/1/9 17:11
 * @version: 1.0
 */
public class Test2 {

    public static void main(String[] args) {
        Thread thread = new Thread();
        thread.start();

    }

    public void RedissonCli(RedissonClient redissonClient){
        RLock rLock = redissonClient.getLock("a");
        RFuture<Void> voidRFuture = rLock.lockAsync();
        rLock.unlock();
        ReentrantLock reentrantLock = new ReentrantLock(false);
        reentrantLock.lock();
        reentrantLock.unlock();
        List<Integer> numberList = Arrays.asList(1,2,3,4,5,6,7,8,9);
        numberList.parallelStream().forEach(System.out::println);
    }
}
