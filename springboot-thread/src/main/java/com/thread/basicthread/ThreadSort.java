package com.thread.basicthread;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * 1、多个线程按顺序执行，调用join()方法
 *      调用join方法，其内部调用了await()方法，此方法需要等待当前线程运行完下面线程才能运行
 * @Author: CPX
 * @Date: 2021/1/6 18:18
 * @version: 1.0
 */
public class ThreadSort {
    public static void main(String[] args){

        Thread thread1 = new Thread(() -> {
            System.out.println("thread1");
        });
        Thread thread2 = new Thread(() -> {
            System.out.println("thread2");
        });
        Thread thread3 = new Thread(() -> {
            System.out.println("thread3");
        });
        Thread thread4 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("thread4");
            }
        });
        try {

            thread1.start();
            thread1.join();
            thread2.start();
            thread2.join();
            thread3.start();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
