package com.lenovo.helloword.restTemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Hello world!
 */

/**
 * 要想让@Async在其他地方生效，需要在启动类上添加@EnableAsync
 * 注：启动类不允许放在src/main/java目录下，需要创建目录
 */
@SpringBootApplication
@EnableAsync
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
        System.out.println("Start Hello World!");
    }
}
