package com.thread.threadpool;

import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * Executors创建方式比较简单，传递参数也比较少
 *  ThreadPoolExecutor创建方式比Executors方式复杂，传递参数多
 * @Author: CPX
 * @Date: 2021/1/7 17:19
 * @version: 1.0
 */
public class ThreadPool {

    public static void createThreadPoolByExecutors(){
        Executors.newCachedThreadPool();
        Executors.newFixedThreadPool(20);
        Executors.newScheduledThreadPool(20);
        Executors.newSingleThreadExecutor();
    }

    public static void createThreadPoolByThreadPoolExecutor(){
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(20, 50, 10, TimeUnit.SECONDS, new SynchronousQueue<>());
        threadPoolExecutor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("利用线程池运行起来了");
            }
        });
        long taskCount = threadPoolExecutor.getTaskCount();
        System.out.println(taskCount);
        System.out.println(threadPoolExecutor.getPoolSize());
        System.out.println(threadPoolExecutor.getCorePoolSize());
        System.out.println(threadPoolExecutor.getKeepAliveTime(TimeUnit.SECONDS));
        threadPoolExecutor.shutdown();
    }

    public static void main(String[] args) {
        createThreadPoolByThreadPoolExecutor();
    }
}
