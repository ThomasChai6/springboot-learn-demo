package com.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * datasource test!
 */
@SpringBootApplication
@EnableAsync
public class DataSourceApp {
    public static void main(String[] args) {
        SpringApplication.run(DataSourceApp.class, args);
        System.out.println("start datasource!");
    }
}
