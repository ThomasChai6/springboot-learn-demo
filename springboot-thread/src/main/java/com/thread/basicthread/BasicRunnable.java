package com.thread.basicthread;

import java.text.SimpleDateFormat;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * Runnable是java.lang包下的
 * @Author: CPX
 * @Date: 2021/1/6 17:29
 * @version: 1.0
 */
public class BasicRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("run able running");
        System.out.println(Thread.currentThread().getName());
    }
}

class WaitTime implements Runnable{
    @Override
    public void run() {
        while (true){
            waitTime(2);
            System.out.println("WaitTime");
        }
    }

    public static final void waitTime(long seconds){
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class WaitingState implements Runnable{
    @Override
    public void run() {
        while (true){
            synchronized (WaitingState.class){
                try {
                    System.out.println("WaitingState");
                    WaitingState.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

class BlockedThread implements Runnable{

    @Override
    public void run() {
        while (true){
            synchronized (BlockedThread.class){
                try {
                    TimeUnit.SECONDS.sleep(2);
                    System.out.println("BlockedThread");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}


class RunnableRun{
    public static void main(String[] args) {
      /*  BasicRunnable basicRunnable = new BasicRunnable();
        basicRunnable.run();
        System.out.println(Thread.currentThread().getName());*/

      /*  WaitTime waitTime = new WaitTime();
        waitTime.run();*/

      /*  WaitingState waitingState = new WaitingState();
        waitingState.run();
        WaitingState waitingState2 = new WaitingState();
        waitingState2.run();*/

 /*       new Thread(new WaitTime(), "WaitingTimeThread").start();
        new Thread(new WaitingState(), "WaitingStateThread").start();
        //BlockedThread-01线程会抢到锁，BlockedThread-02线程会阻塞
        new Thread(new BlockedThread(), "BlockedThread-01").start();
        new Thread(new BlockedThread(), "BlockedThread-02").start();*/

    }
}
